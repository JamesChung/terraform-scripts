locals {
    create_org_file = "${path.module}/src/create_org_analyzer.py"
    create_rules_file = "${path.module}/src/create_org_analyzer_rules.py"
    delete_org_file = "${path.module}/src/delete_org_analyzer.py"
}

resource "null_resource" "create_org_analyzer" {
    provisioner "local-exec" {
        command = local.create_org_file
        interpreter = ["python3"]
    }

    triggers = {
      "hashval" = filebase64sha256(local.create_org_file)
    }
}

resource "null_resource" "create_analyzer_rules" {
    provisioner "local-exec" {
        command = local.create_rules_file
        interpreter = ["python3"]
    }

    depends_on = [
        null_resource.create_org_analyzer
    ]

    triggers = {
      "hashval" = filebase64sha256(local.create_rules_file)
    }
}

resource "null_resource" "destroy_org_analyzer" {
    provisioner "local-exec" {
        command = local.delete_org_file
        interpreter = ["python3"]
        when = destroy
    }

    triggers = {
      "hashval" = filebase64sha256(local.delete_org_file)
    }
}
