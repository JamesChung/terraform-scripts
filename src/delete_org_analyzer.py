import boto3


def main():
    accessanalyzer = boto3.client("accessanalyzer", region_name="us-east-1")
    delete_analyzer(accessanalyzer)


def delete_analyzer(client):
    try:
        response = client.delete_analyzer(
            analyzerName="OrganizationsAnalyzer"
        )
    except client.exceptions.ResourceNotFoundException as re:
        print(re)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    main()
