import boto3


def main():
    accessanalyzer = boto3.client("accessanalyzer", region_name="us-east-1")
    create_analyzer(accessanalyzer)


def create_analyzer(client):
    try:
        response = client.create_analyzer(
            analyzerName="OrganizationsAnalyzer",
            type="ORGANIZATION"
        )
        print(response)
    except client.exceptions.ConflictException:
        print("Analyzer already exists. Skipping...")
    except Exception as e:
        print(e)


if __name__ == "__main__":
    main()
