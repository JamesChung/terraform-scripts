import boto3


def main():
    accessanalyzer = boto3.client("accessanalyzer", region_name="us-east-1")
    create_archive_rules(accessanalyzer, create_rules())


def create_archive_rules(client, rules):
    try:
        for rule in rules:
            response = client.create_archive_rule(
                analyzerName='OrganizationsAnalyzer',
                filter=rule["filter"],
                ruleName=rule["ruleName"]
            )
    except client.exceptions.ConflictException as ce:
        print(ce)
    except Exception as e:
        print(e)


def create_rules():
    rules = []
    rules.append(
        {
            "ruleName": "Mytest",
            "filter": {
                "principal.AWS": {
                    "eq": [
                        "222385417670"
                    ]
                },
                "resourceType": {
                    "eq": [
                        "AWS::IAM::Role"
                    ]
                }
            }
        }
    )
    return rules


if __name__ == "__main__":
    main()
